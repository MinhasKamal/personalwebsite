function initialize() {
	var myStyles = [
		{
			featureType: "all",
			elementType: "all",
			stylers: [
				{hue: '#48B3C0'},
				{saturation: -50},
				{lightness: 30},
				{gamma: 0.5}
			]
		},
	];

	var mapInit={
		center:new google.maps.LatLng(23.822852, 90.364186),
		zoom:18,
		styles: myStyles,
		mapTypeId:google.maps.MapTypeId.ROADMAP
	};
	var map=new google.maps.Map(document.getElementById("googleMap"), mapInit);
	marker = new google.maps.Marker({map: map,position: new google.maps.LatLng(23.823100, 90.364150)});
}

google.maps.event.addDomListener(window, 'load', initialize);