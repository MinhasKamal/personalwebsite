function validation(){
	$("#first_name_alert").html("");
	$("#last_name_alert").html("");
	$("#email_alert").html("");
	$("#message_alert").html("");
	
	var alphabets = /^[A-Z a-z]+$/;  
	if( (document.send_message.first_name.value.length<3) || 
		(!document.send_message.first_name.value.match(alphabets)) ) {
		//alert("Enter your first name correctly.");
		$("#first_name_alert").html("Enter your first name correctly.");
		document.send_message.first_name.focus();
		return false;
    }
	
	if( (document.send_message.last_name.value.length<3) || 
		(!document.send_message.last_name.value.match(alphabets)) ) {
		$("#last_name_alert").html("Enter your last name correctly.");
		document.send_message.last_name.focus();
		return false;
    }
	
	var mail_format = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/; 
	if(!document.send_message.email.value.match(mail_format)) {
        $("#email_alert").html("This email address is invalid.");
        document.send_message.email.focus();
        return false;
    }
	
	if(document.send_message.message.value.length<15) {
		$("#message_alert").html("Please Enter a complete message (more than 15 characters).");
		document.send_message.message.focus();
		return false;
    }
	
	return true;
}